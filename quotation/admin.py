from django.contrib import admin

from .models import Quotation


class QuotationAdmin(admin.ModelAdmin):
    list_display = ('title', 'customer', 'status', 'date_created',)
    list_filter = ('status', 'date_created',)
    search_fields = ('customer',)
    date_hierarchy = 'date_created'
    ordering = ['status', '-date_created', ]
    raw_id_fields = ('customer',)
    exclude = ('date_created', 'date_updated',)


admin.site.register(Quotation, QuotationAdmin)

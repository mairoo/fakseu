# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-24 16:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quotation', '0002_auto_20170725_0045'),
    ]

    operations = [
        migrations.AddField(
            model_name='quotation',
            name='title',
            field=models.CharField(default=' ', max_length=250, verbose_name='Title'),
            preserve_default=False,
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-24 15:45
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quotation', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='quotation',
            options={'ordering': ('-date_created',), 'verbose_name': 'quotation', 'verbose_name_plural': 'quotation'},
        ),
    ]

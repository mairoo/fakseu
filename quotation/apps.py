from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class QuotationConfig(AppConfig):
    name = 'quotation'
    verbose_name = _('Quotation')

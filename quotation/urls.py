from django.conf.urls import url

from .views import (QuotationCreateView, QuotationDetailView)

urlpatterns = [
    url(
        r'^new/$',
        QuotationCreateView.as_view(),
        name='quotation-new'
    ),
    url(
        r'(?P<pk>\d+)/$',
        QuotationDetailView.as_view(),
        name='quotation-detail'
    ),
]

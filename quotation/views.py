from django.core.urlresolvers import reverse
from django.views.generic import DetailView
from django.views.generic.edit import CreateView

from .forms import QuotationForm
from .models import Quotation


class QuotationCreateView(CreateView):
    template_name = 'quotation/quotation_form.html'
    form_class = QuotationForm

    def get_form_kwargs(self):
        # overrides to pass 'self.request' object
        kwargs = super(QuotationCreateView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        form.instance.customer = self.request.user
        return super(QuotationCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('quotation:quotation-detail', args=(self.object.id,))


class QuotationDetailView(DetailView):
    template_name = 'quotation/quotation_detail.html'
    context_object_name = 'quotation'
    model = Quotation

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Quotation(models.Model):
    DRAFT = 0
    CONFIRMED = 1
    PAID = 2
    SHIPPED = 3

    STATUS_CHOICES = (
        (DRAFT, _('Draft')),
        (CONFIRMED, _('Confirmed')),
        (PAID, _('Paid')),
        (SHIPPED, _('Shipped')),
    )

    customer = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Customer'),
        related_name='quotations'
    )
    title = models.CharField(
        verbose_name=_('Title'),
        max_length=250
    )
    status = models.IntegerField(
        verbose_name=_('Status'),
        choices=STATUS_CHOICES,
        default=DRAFT
    )
    date_created = models.DateTimeField(
        verbose_name=_('Date created'),
        auto_now_add=True
    )
    date_updated = models.DateTimeField(
        verbose_name=_('Date updated'),
        auto_now=True
    )

    class Meta:
        verbose_name = _('quotation')
        verbose_name_plural = _('quotation')
        ordering = ('-date_created',)

    def __str__(self):
        return '%s %s' % (self.pk, self.customer_id)

    def get_absolute_url(self):
        return reverse('quotation:quotation-detail', args=(self.pk, self.slug,))
